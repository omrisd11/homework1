#include "queue.h"
#include <iostream>

void mergeSort(int* arr, int begin, int end)
{
	int temp;
	int i;
	int first;
	int second;
	bool endLoop = false;
	bool useFirst = false;
	bool useSecond = false;
	queue* firstHalf;
	queue* secondHalf;
	if (end - begin <= 1)
	{
		if (arr[begin] > arr[end])
		{
			temp = arr[begin];
			arr[begin] = arr[end];
			arr[end] = temp;
		}
	}
	else
	{
		mergeSort(arr, begin, (end + begin) / 2);
		mergeSort(arr, (end + begin) / 2 + 1, end);
		firstHalf = new queue;
		initQueue(firstHalf);
		secondHalf = new queue;
		initQueue(secondHalf);
		for (i = begin; i <= (end + begin) / 2; i++)
		{
			enqueue(firstHalf, arr[i]);
		}
		for (i = (end + begin) / 2 + 1; i <= end; i++)
		{
			enqueue(secondHalf, arr[i]);
		}

		if (!isEmpty(firstHalf))
		{
			first = dequeue(firstHalf);
		}
		if (!isEmpty(secondHalf))
		{
			second = dequeue(secondHalf);
		}
		for (i = begin; i <= end && !endLoop; i++)
		{
			if (first < second)
			{
				arr[i] = first;
				if (isEmpty(firstHalf))
				{
					endLoop = true;
					useSecond = true;
				}
				else
				{
					first = dequeue(firstHalf);
				}
			}
			else
			{
				arr[i] = second;
				if (isEmpty(secondHalf))
				{
					endLoop = true;
					useFirst = true;
				}
				else
				{
					second = dequeue(secondHalf);
				}
			}
		}

		if (useFirst)
		{
			arr[i] = first;
			i++;
		}
		else
		{
			if (useSecond)
			{
				arr[i] = second;
				i++;
			}
		}
		while (!isEmpty(firstHalf))
		{
			arr[i] = dequeue(firstHalf);
			i++;
		}

		while (!isEmpty(secondHalf))
		{
			arr[i] = dequeue(secondHalf);
			i++;
		}
	}
}