#include "stack.h"

void push(myStack *s, int element)
{
	addToList(&(s->li), element);
	s->count++;
	s->empty = false;
}

int pop(myStack *s)
{
	int ans=deleteFromList(&(s->li));
	if (s->count > 0)
	{
		s->count--;
	}
	if (s->count == 0)
	{
		s->empty = true;
	}
	return ans;
}

void initStack(myStack *s)
{
	s->count = 0;
	s->empty = true;
	s->li = nullptr;
}

bool isEmpty(myStack *s)
{
	return s->empty;
}

void cleanStack(myStack *s)
{
	list* temp;
	while (s->li!=nullptr)
	{
		temp = s->li;
		s->li = s->li->next;
		delete temp;
	}
}