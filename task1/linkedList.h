struct list
{
	int num;
	struct list* next;
};

typedef struct list list;

void addToList(list** li, int num);
int deleteFromList(list** li);