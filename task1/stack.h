#include "linkedList.h"

struct myStack
{
	int count;
	bool empty;
	list* li;
};

typedef struct myStack myStack;

void push(myStack *s, int element);
int pop(myStack *s);
void initStack(myStack *s);
bool isEmpty(myStack *s);
void cleanStack(myStack *s);

