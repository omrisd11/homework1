#include  "linkedList.h"

void addToList(list** li, int num)
{
	list* newNode = new list;
	newNode->num = num;
	newNode->next = *li;
	*li = newNode;
}

int deleteFromList(list** li)
{
	int ans = -1;
	list* temp;
	if (*li != nullptr)
	{
		ans = (*li)->num;
		temp = *li;
		*li = (*li)->next;
		delete temp;
	}
	return ans;
}