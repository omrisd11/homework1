#include <iostream>
#include "stack.h"

void reverse(int* nums, int size);

int main()
{
	int i;
	int* nums = new int[10];
	std::cout << "Please enter 10 numbers : " << std::endl;
	for (i = 0; i < 10; i++)
	{
		std::cin >> nums[i];
	}
	reverse(nums, 10);
	for (i = 0; i < 10; i++)
	{
		std::cout << nums[i] << std::endl;
	}
	getchar();
	getchar();
	return 0;
	
}

void reverse(int* nums, int size)
{
	int i;
	myStack* s = new myStack;
	initStack(s);
	for (i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}
	for (i = 0; i < size; i++)
	{
		nums[i] = pop(s);
	}
	cleanStack(s);
	delete s;
}