#include "stack.h"

struct queue
{
	myStack* st;
	myStack* temp;
	bool empty;
};

typedef struct queue queue;

void initQueue(queue* q);
void enqueue(queue* q, int element);
int dequeue(queue* q);
void cleanQueue(queue* q);
bool isEmpty(queue* q);