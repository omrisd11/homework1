#include "queue.h"

void initQueue(queue* q)
{
	q->st = new myStack;
	initStack(q->st);
	q->temp = new myStack;
	initStack(q->temp);
	q->empty = true;
}

void enqueue(queue* q, int element)
{
	push(q->st, element);
	q->empty = false;
}

int dequeue(queue* q)
{
	int ans = -1;
	while (!isEmpty(q->st))
	{
		push(q->temp, pop(q->st));
	}
	if (!isEmpty(q->temp))
	{
		ans = pop(q->temp);
	}
	while (!isEmpty(q->temp))
	{
		push(q->st, pop(q->temp));
	}
	if (isEmpty(q->st))
	{
		q->empty = true;
	}
	return ans;
}

void cleanQueue(queue* q)
{
	cleanStack(q->st);
	delete q->st;
	cleanStack(q->temp);
	delete q->temp;
}

bool isEmpty(queue* q)
{
	return q->empty;
}